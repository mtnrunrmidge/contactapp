//
//  ViewController.swift
//  ContactsApp
//
//  Created by Margaret Schroeder on 10/13/17.
//  Copyright © 2017 Margaret Schroeder. All rights reserved.
//

import UIKit
import RealmSwift
import MessageUI

class ViewController: UIViewController, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate{
    //dismiss sms
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    //dismiss email
    func mailComposeController(controller: MFMailComposeViewController,
                               didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        // Check the result or perform other tasks.
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
    //https://realm.io/docs/swift/latest/
    //https://www.raywenderlich.com/112544/realm-tutorial-getting-started
    //Migrations - https://stackoverflow.com/questions/34891743/realm-migrations-in-swift
    
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //phone calling
        if let url = URL(string: "tel://\(8018857384)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
        //text message
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = "Hello this is a test"
            controller.recipients = ["8018857384"]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    //email
    if(MFMailComposeViewController.canSendMail()){
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        // Configure the fields of the interface.
        composeVC.setToRecipients(["midgeschroeder@gmail.com"])
        composeVC.setSubject("Hello! This is a test.")
        composeVC.setMessageBody("Hello this is my message body!", isHTML: false)
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
        
        //maps
    
        
//        var categories: Results<Category> = {self.realm.objects(Category.self)}()
//
//        //create
//        try! realm.write{
//            let defaultCategories = [ "Birds", "Mammals", "Flora", "Reptiles", "Arachnids"]
//            for category in defaultCategories{
//                let newCategory = Category()
//                newCategory.name = category
//                self.realm.add(newCategory)
//            }
//        }
//
//        //update
//        try! realm.write{
//            for category in categories{
//                category.name = "test"
//                category.secondName = "Second"
//            }
//        }
//        categories = realm.objects(Category.self)
//
//        //delete category
//        try! realm.write{
//            for category in categories{
//                realm.delete(category)
//            }
//        }
//        categories = realm.objects(Category.self)
//        print(categories)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


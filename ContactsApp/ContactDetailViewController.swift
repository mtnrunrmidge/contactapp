//
//  ContactDetailViewController.swift
//  ContactsApp
//
//  Created by Margaret Schroeder on 10/13/17.
//  Copyright © 2017 Margaret Schroeder. All rights reserved.
//

import UIKit
import RealmSwift
import MessageUI

class ContactDetailViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

     @IBAction func placeCall(_ sender: Any) {
        performSegue(withIdentifier: "placeCall", sender: self)
     }
    
    @IBAction func placeSMS(_ sender: Any) {
        performSegue(withIdentifier: "sendSMS", sender: self)
    }
    
    @IBAction func sendEmail(_ sender: Any) {
        performSegue(withIdentifier: "sendEmail", sender: self)
    }
    /*
     // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

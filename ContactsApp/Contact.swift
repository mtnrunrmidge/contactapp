//
//  Contact.swift
//  ContactsApp
//
//  Created by Margaret Schroeder on 10/13/17.
//  Copyright © 2017 Margaret Schroeder. All rights reserved.
//

import Foundation
import RealmSwift


struct Contact{
    let name : String
    let phoneNumber : String
    let email : String
    let address : String
}

//class Contact : Object {
//    dynamic var name = ""
//    dynamic var phoneNum = ""
//    dynamic var emailAdd = ""
//    dynamic var address = ""
//}


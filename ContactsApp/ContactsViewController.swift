//
//  ContactsViewController.swift
//  ContactsApp
//
//  Created by Margaret Schroeder on 10/13/17.
//  Copyright © 2017 Margaret Schroeder. All rights reserved.
//

import UIKit
import RealmSwift
import MessageUI

class ContactsViewController: UITableViewController, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate
{
    //dismiss sms
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    //dismiss email
    func mailComposeController(controller: MFMailComposeViewController,
                               didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        // Check the result or perform other tasks.
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
    //https://realm.io/docs/swift/latest/
    //https://www.raywenderlich.com/112544/realm-tutorial-getting-started
    //Migrations - https://stackoverflow.com/questions/34891743/realm-migrations-in-swift
    
    let realm = try! Realm()
    var contacts: [Contact] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //tableView.reloadData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //Mark - data
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath)
        let contact = contacts[indexPath.row]
        
        cell.textLabel?.text = contact.name
        cell.detailTextLabel?.text = contact.phoneNumber
        return cell
    }
    
    @IBAction func unwindToContactList(segue: UIStoryboardSegue){
        guard let viewController = segue.source as? AddContactViewController else {return}
        guard let name = viewController.NameTextField.text, let phoneNumber = viewController.PhoneTextField.text, let email = viewController.EmailTextField.text, let address = viewController.AddressTextField.text else { return }
        let contact = Contact(name: name, phoneNumber: phoneNumber, email: email, address: address)
        contacts.append(contact)
        tableView.reloadData()
    }
    
    @IBAction func placeCall(segue: UIStoryboardSegue){
        if let url = URL(string: "tel://\(8018857384)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBAction func sendSMS(segue: UIStoryboardSegue){
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = "Hello this is a test"
            controller.recipients = ["8018857384"]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    @IBAction func sendEmail(segue: UIStoryboardSegue){
        //email
        if(MFMailComposeViewController.canSendMail()){
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            // Configure the fields of the interface.
            composeVC.setToRecipients(["midgeschroeder@gmail.com"])
            composeVC.setSubject("Hello! This is a test.")
            composeVC.setMessageBody("Hello this is my message body!", isHTML: false)
            // Present the view controller modally.
            self.present(composeVC, animated: true, completion: nil)
        }
    }
}

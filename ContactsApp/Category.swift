//
//  Category.swift
//  ContactsApp
//
//  Created by Margaret Schroeder on 10/13/17.
//  Copyright © 2017 Margaret Schroeder. All rights reserved.
//

import Foundation
import RealmSwift

class Category : Object {
    dynamic var name = ""
    dynamic var secondName = ""
}
